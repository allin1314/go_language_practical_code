package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const (
	numberGoroutines = 4
	taskLoad         = 10
)

var wg7 sync.WaitGroup

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	tasks := make(chan string, taskLoad)

	wg7.Add(numberGoroutines)
	for gr := 1; gr <= numberGoroutines; gr++ {
		go worker(tasks, gr)
	}

	for post := 1; post <= taskLoad; post++ {
		fmt.Println("set number: >>>", post)
		tasks <- fmt.Sprintf("Task : %d", post)
	}

	close(tasks)
	wg7.Wait()
}

func worker(tasks chan string, worker int) {
	defer wg7.Done()

	for {
		task, ok := <-tasks
		if !ok {
			fmt.Printf("--->>> done Worder: %d: Shutting Down\n", worker)
			return
		}

		fmt.Printf("Worker: %d: Shutting %s\n", worker, task)

		//sleep := rand.Int63n(10)
		//time.Sleep(time.Duration(sleep) * time.Millisecond)
		fmt.Printf("Worker: %d: Completed %s\n", worker, task)
	}
}
