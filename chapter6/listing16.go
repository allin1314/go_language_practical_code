package main

import (
	"fmt"
	"runtime"
	"sync"
)

var (
	counter3 int
	wg3      sync.WaitGroup
	mutex    sync.Mutex
)

func main() {
	wg3.Add(2)
	go incCounter2(1)
	go incCounter2(2)

	wg3.Wait()
	fmt.Printf("Final Counter: %d\n", counter3)
}

func incCounter2(id int64) {
	defer wg3.Done()

	for count := 0; count < 2; count++ {
		mutex.Lock()
		{
			value := counter3
			runtime.Gosched()
			value++
			counter3 = value
		}
		mutex.Unlock()
	}
}
