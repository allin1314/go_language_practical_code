package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
)

var (
	counter1 int64
	wg2      sync.WaitGroup
)

func main() {
	wg2.Add(2)
	go incCounter1(1)
	go incCounter1(2)

	wg2.Wait()

	fmt.Println("Final Counter:", counter1)
}

func incCounter1(id int64) {
	defer wg2.Done()
	for count := 0; count < 2; count++ {
		atomic.AddInt64(&counter1, 1)
		runtime.Gosched()
	}
}
