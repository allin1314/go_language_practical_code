package main

import (
	"fmt"
	"runtime"
	"sync"
)

var (
	// counter 是所有goroutine都要增加其值的变量
	counter int

	// wg用来等待程序结束
	wg1 sync.WaitGroup
)

func main() {
	wg1.Add(2)

	go incCounter()
	go incCounter()

	wg1.Wait()
	fmt.Println("Final Counter:", counter)
	run
}

// incCounter增加包里counter变量的值
func incCounter() {
	defer wg1.Done()

	for count := 0; count < 2; count++ {
		// 捕获counter的值
		value := count

		// 当前goroutine从线程退出，并放回到队列
		runtime.Gosched()

		// 增加本地value变量的值
		value++

		// 将该值保存回counter
		counter = value
	}
}
