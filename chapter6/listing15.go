package main

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

var (
	shutdown int64
	wgs      sync.WaitGroup
)

func main() {
	wgs.Add(2)
	go doWork("A")
	go doWork("B")

	time.Sleep(1 * time.Second)
	fmt.Println("Shutdown Now")
	atomic.StoreInt64(&shutdown, 1)
	wgs.Wait()
}

func doWork(name string) {
	defer wgs.Done()
	for {
		fmt.Printf("Doing %s Work", name)
		time.Sleep(250 * time.Millisecond)

		if atomic.LoadInt64(&shutdown) == 1 {
			fmt.Printf("shutting %s Down", name)
			break
		}
	}
}
