package main

import "fmt"

type notifier1 interface {
	notify()
}

type User struct {
	name  string
	email string
}

func (u *User) notify() {
	fmt.Printf("Sending user email to %s<%s>\n", u.name, u.email)
}

type admin struct {
	name  string
	email string
}

func (a *admin) notify() {
	fmt.Printf("Sending user email to %s<%s>\n", a.name, a.email)
}

//func main() {
//	bill := User{"Bill", "bill@email.com"}
//	sendNotification(&bill)
//
//	allin := admin{"allin", "allin@email.com"}
//	sendNotification(&allin)
//}
//
//func sendNotification(n notifier) {
//	n.notify()
//}
