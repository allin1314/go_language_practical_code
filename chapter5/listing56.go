package main

import "fmt"

type notifier interface {
	notify()
}

type user struct {
	name  string
	email string
}

type admin21 struct {
	user
	level string
}

func (u *user) notify() {
	fmt.Printf("Sending user email to %s<%s>\n", u.name, u.email)
}

//func main() {
//	ad := admin21{
//		user: user{
//			name: "allin",
//			email: "allin@email.com",
//		},
//		level: "supper",
//	}
//	sendNotification(&ad)
//}
//
func sendNotification(n notifier) {
	n.notify()
}
