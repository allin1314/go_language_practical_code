package main

import (
	"fmt"
)

type duration int

func (d *duration) pretty() string {
	return fmt.Sprintf("Duration: %d", *d)
}

//func main() {
//	a := 10
//	time.Sleep(time.Duration(a) * time.Second)
//	//duration(42).pretty()
//}
