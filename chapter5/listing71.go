package main

import (
	"fmt"
	"myProject/goinaction/chapter5/entities"
)

//func main() {
//	u := entities.User{
//		Name: "Bill",
//		Email: "1223",
//	}
//	fmt.Printf("User: %v\n", u)
//}

func main() {
	a := entities.Admin{
		Rights: 10,
	}
	a.Name = "Bill"
	a.Email = "123@qq.com"
	fmt.Printf("User: %v\n", a)
}
