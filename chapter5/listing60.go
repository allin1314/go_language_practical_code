package main

import "fmt"

type notifier2 interface {
	notify1()
}

type user2 struct {
	name  string
	email string
}

func (u *user) notify1() {
	fmt.Printf("Sending user email to %s<%s>\n", u.name, u.email)
}

type admin2 struct {
	user
	level string
}

func (a *admin2) notify1() {
	fmt.Printf("Sending admin email to %s<%s>\n", a.name, a.email)
}

//func main() {
//	adc := admin2{
//		user:  user{
//			name: "alex",
//			email: "123@qq.com",
//		},
//		level: "supper",
//	}
//
//	sendNotification1(&adc)
//	adc.user.notify1()
//	adc.notify1()
//}
//
//func sendNotification1(n notifier2) {
//	n.notify1()
//}
