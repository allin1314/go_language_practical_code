package main

import (
	"log"
	_ "myProject/goinaction/chapter2/matchers"
	"myProject/goinaction/chapter2/search"
	"os"
)

// init在main之前调用
func init() {
	// 将日志输出到标准输出
	log.SetOutput(os.Stdout)
}

// main 是程序的入口
func main() {
	search.Run("president")
}
