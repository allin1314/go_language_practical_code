package main

import (
	"fmt"
)

func main() {
	slice := []int{10, 20, 30, 40, 50}
	newSlice := slice[1:3]
	//fmt.Println(newSlice, len(newSlice), cap(newSlice))

	newSlice = append(newSlice, 60)
	fmt.Println(newSlice, len(newSlice), cap(newSlice))
	fmt.Println(slice, len(slice), cap(slice))

	newSlice = append(newSlice, 60, 70, 80)
	fmt.Println(newSlice, len(newSlice), cap(newSlice))
	fmt.Println(slice, len(slice), cap(slice))
}
